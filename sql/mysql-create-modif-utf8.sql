/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  racin
 * Created: 6 oct. 2020
 */

DROP TABLE IF EXISTS `Lieu`;
DROP TABLE IF EXISTS `Representation`;

create table Lieu 
(id char(3)not null,
nom varchar (50) not null,
adresse varchar(50) not null,
cptAccueil int not null,
constraint pk_Lieu primary key(id))
ENGINE=INNODB;

create table Representation
(id char(3)not null,
date date not null,
heureDebut time not null,
heureFin time not null,
idGroupe char(4) not null,
idLieu char(4) not null,
constraint pk_Representation primary key(id),
constraint fk1_Representation foreign key(idGroupe) references Groupe(id),
constraint fk2_Representation foreign key(idLieu) references Lieu(id))
ENGINE=INNODB;