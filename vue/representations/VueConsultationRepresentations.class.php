<?php

namespace vue\representations;

use vue\VueGenerique;
use modele\metier\Groupe;
use modele\dao\RepresentationDAO;

class VueConsultationRepresentations extends VueGenerique {

    /**
     * 
     * @var array 
     * liste des représentations 
     */
    private $lesRepresentations;

    /**
     * @var array 
     * liste des groupes 
     */
    private $lesGroupes;

    /**
     * @var array 
     * liste des lieux 
     */
    private $lesLieux;

    /**
     * @var array 
     * liste des date de représentations 
     */
    private $lesDates;

    /**
     * @var array 
     * liste des heures du debut de la représentation 
     */
    private $lesHeuresDeb;

    /**
     * @var array 
     * liste des heures de fin de la représentation 
     */
    private $lesHeuresFin;

    /**
     * @var Groupe 
     */
    private $unGroupe;

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();

        if (count($this->lesGroupes) != 0 && count($this->lesLieux) != 0) {
            $tabDate = array();
            foreach ($this->lesRepresentations as $uneRepresentation) {
                $date = $uneRepresentation->getDate();
                $ok = in_array($date, $tabDate);
                if (!$ok) {
                    array_push($tabDate, $date);
                }
            }
            ?>
            <h2>Programme par jour</h2>
            <?php
            foreach ($tabDate as $date) {
                ?>
                <strong><?= $date ?></strong><br>
                <table width="45%" cellspacing="0" class="tabQuadrille">
                    <tr class="enTeteTabQuad">
                        <td width="25%">Lieu</td>
                        <td width="25%">Groupe</td>
                        <td width="25%">Heure de début</td>
                        <td width="25%">Heure de fin</td>
                        <td width="25%">Modifier</td>
                        <td width="25%">Supprimer</td>
                        
                        
                    </tr>
                    <?php
                    $lesRepresentations2 = RepresentationDAO::getAllByDate($date);
                    foreach ($lesRepresentations2 as $uneRepresentation2) {
                        $id = $uneRepresentation2->getId();
                        ?>
                        <tr class="ligneTabQuad">
                            <td><?= $uneRepresentation2->getUnLieu()->getNom() ?></td>
                            <td><?= $uneRepresentation2->getUnGroupe()->getNom() ?></td>
                            <td><?= $uneRepresentation2->getHeureDebut() ?></td>
                            <td><?= $uneRepresentation2->getHeureFin() ?></td>
                            <td width="16%" align="center" > 
                                <a href="index.php?controleur=representations&action=modifier&id=<?= $id ?>" >
                                    Modifier
                                </a>
                            </td>
                            <td width="16%" align="center" > 
                                <a href="index.php?controleur=representations&action=supprimer&id=<?= $id ?>" >
                                    Supprimer
                                </a>
                            </td>
                        </tr> 
                        <?php
                    }
                    ?>

                </table><br>
                <?php
            }
        }
        ?>
        <br/>
        
        <a href="index.php?controleur=representations&action=creer" >
            Création d'une représentation</a >
        
        <?php
        include $this->getPied();
    }

    function setLesRepresentations(array $lesRepresentations) {
        $this->lesRepresentations = $lesRepresentations;
    }

    function setLesGroupes(array $lesGroupes) {
        $this->lesGroupes = $lesGroupes;
    }

    function setLesLieux(array $lesLieux) {
        $this->lesLieux = $lesLieux;
    }

    function setLesDates(array $lesDates) {
        $this->lesDates = $lesDates;
    }

}
?>

