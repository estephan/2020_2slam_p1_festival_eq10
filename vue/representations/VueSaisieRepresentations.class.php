<?php

namespace vue\representations;

use vue\VueGenerique;
use modele\metier\Representation;



class VueSaisieRepresentations extends VueGenerique {

   /**
     * 
     * @var array 
     * liste des représentations 
     */
    private $lesRepresentations;

    /**
     * @var array 
     * liste des groupes 
     */
    private $lesGroupes;

    /**
     * @var array 
     * liste des lieux 
     */
    private $lesLieux;

    /**
     * @var array 
     * liste des date de représentations 
     */
    private $lesDates;

    /**
     * @var array 
     * liste des heures du debut de la représentation 
     */
    private $lesHeuresDebut;

    /**
     * @var array 
     * liste des heures de fin de la représentation 
     */
    private $lesHeuresFin;

    /**
     * @var Groupe 
     */
    private $unGroupe;
    
    private $uneRepresentation;


    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();
        ?>
        <form method="POST" action="index.php?controleur=representations&action=<?= $this->actionAEnvoyer ?>">
            <br>
            <table width="40%" cellspacing="0" cellpadding="0" class="tabNonQuadrille">

                <tr class="enTeteTabNonQuad">
                    <td colspan="3"><strong><?= $this->message ?></strong></td>
                </tr>

                <?php
                // En cas de création, l'id est accessible à la saisie           
                if ($this->actionRecue == "creer") {
                    // On a le souci de ré-afficher l'id tel qu'il a été saisi
                    ?>
                    <tr class="ligneTabNonQuad">
                        <td> Id*: </td>
                        <td><input type="text" placeholder="Ex: R01" value="<?= $this->uneRepresentation->getId() ?>" name="id" size ="10" maxlength="8"></td>
                    </tr>
                    <?php
                } else {
                    // sinon l'id est dans un champ caché 
                    ?>
                    <tr>
                        <td><input type="hidden" value="<?= $this->uneRepresentation->getId(); ?>" name="id"></td><td></td>
                    </tr>
                    <?php
                }
                ?>
                <tr class="ligneTabNonQuad">
                    <td> Id du lieu*: </td>
                    <td><input type="text" required placeholder="Ex: L03" value="<?= $this->uneRepresentation->getUnLieu()->getId() ?>" name="lieu" size="50" 
                               maxlength="45"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Id du groupe*: </td>
                    <td><input type="text" required placeholder="Ex: G012" value="<?= $this->uneRepresentation->getUnGroupe()->getId() ?>" name="groupe" 
                               size="50" maxlength="45"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Heure de début*: </td>
                    <td><input type="text" placeholder="HH:MM:SS" value="<?= $this->uneRepresentation->getHeureDebut() ?>" name="heuredebut" 
                               size="50" maxlength="40"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Heure de fin*: </td>
                    <td><input type="text" placeholder="HH:MM:SS" value="<?= $this->uneRepresentation->getHeureFin() ?>" name="heurefin" size="50" 
                               maxlength="40"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Date*: </td>
                    <td><input type="text" placeholder="AAAA-MM-JJ" value="<?= $this->uneRepresentation->getDate() ?>" name="date" size="50" 
                               maxlength="40"></td>
                </tr>
                
                
            </table>

            <table align="center" cellspacing="15" cellpadding="0">
                <tr>
                    <td align="right"><input type="submit" value="Valider" name="valider">
                    </td>
                    <td align="left"><input type="reset" value="Annuler" name="annuler">
                    </td>
                </tr>
            </table>
            <a href="index.php?controleur=representations&action=defaut">Retour</a>
        </form>
        <?php
        include $this->getPied();
    }

    public function setUneRepresentation(Representation $uneRepresentation) {
        $this->uneRepresentation = $uneRepresentation;
    }

    public function setActionRecue(string $action) {
        $this->actionRecue = $action;
    }

    public function setActionAEnvoyer(string $action) {
        $this->actionAEnvoyer = $action;
    }

    public function setMessage(string $message) {
        $this->message = $message;
    }

}
