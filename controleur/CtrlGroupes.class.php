<?php

/**
 * Contrôleur de gestion des groupes
 * @author prof
 * @version 2018
 */

namespace controleur;

use controleur\GestionErreurs;
use modele\dao\GroupeDAO;
use modele\dao\AttributionDAO;
use modele\metier\Groupe;
use modele\dao\Bdd;
use vue\groupes\VueListeGroupes;
use vue\groupes\VueDetailGroupes;
use vue\groupes\VueSaisieGroupe;
use vue\groupes\VueSupprimerGroupe;

class CtrlGroupes extends ControleurGenerique {

    /** controleur= groupes & action= defaut
     * Afficher la liste des groupes      */
    public function defaut() {
        $this->liste();
    }

    /** controleur= groupes & action= liste
     * Afficher la liste des groupes      */
    public function liste() {
        $laVue = new VueListeGroupes();
        $this->vue = $laVue;
        // On récupère un tableau composé de la liste des groupes avec, pour chacun,
        //  son nombre d'attributions de chambres actuel : 
        //  on ne peut supprimer un groupe que si aucune chambre ne lui est actuellement attribuée
        Bdd::connecter();
        $laVue->setLesGroupesAvecNbAttributions($this->getTabGroupesAvecNbAttributions());
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - groupes");
        $this->vue->afficher();
    }
        /** controleur= groupes & action=detail & id=identifiant_groupe
     * Afficher un groupe d'après son identifiant     */
    public function detail() {
        $idGroupe = $_GET["id"];
        $this->vue = new VueDetailGroupes();
        // Lire dans la BDD les données du groupe à afficher
        Bdd::connecter();
        $this->vue->setUnGroupe(GroupeDAO::getOneById($idGroupe));
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - groupes");
        $this->vue->afficher();
    }
        /**
     * Retourne la liste de tous les Groupes et du nombre d'attributions de chacun
     * @return Array tableau associatif à 2 dimensions : 
     *      - dimension 1, l'index est l'id du groupe
     *      - dimension 2, index "etab" => objet de type Groupe
     *      - dimension 2, index "nbAttrib" => nombre d'attributions pour cet groupe
     */
    public function getTabGroupesAvecNbAttributions(): Array {
        $lesGroupesAvecNbAttrib = Array();
        $lesGroupes = GroupeDAO::getAll();
        foreach ($lesGroupes as $unGroupe) {
            /* @var Groupe $unGroupe */
            $lesGroupesAvecNbAttrib[$unGroupe->getId()]['groupe'] = $unGroupe;
            //$lesGroupesAvecNbAttrib[$unGroupe->getId()]['nbGroupe'] = count(AttributionDAO::getAllByIdGroup($unGroupe->getId()));
        }
        return $lesGroupesAvecNbAttrib;
    }
    
    public function supprimerGroupe() {
        $idGrp = $_GET["id"];
        $this->vue = new VueSupprimerGroupe();
        // Lire dans la BDD les données du groupe à supprimer
        Bdd::connecter();
        $this->vue->setUnGroupe(GroupeDAO::getOneById($idGrp));
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Groupes");
        $this->vue->afficher();
    }
    
     /** controleur= groupe & action= validerSupprimerGroupe
     * supprimer un groupe dans la base de données après confirmation   */
    public function validerSupprimer() {
        Bdd::connecter();
        if (!isset($_GET["id"])) {
            // pas d'identifiant fourni
            GestionErreurs::ajouter("Il manque l'identifiant du groupe à supprimer");
        } else {
            // suppression de l'groupe d'après son identifiant
            GroupeDAO::delete($_GET["id"]);
        }
        // retour à la liste des groupes
        header("Location: index.php?controleur=groupes&action=liste");
    }
    
    /** controleur= groupes & action=creer
     * Afficher le formulaire d'ajout d'un groupe     */
    public function creer() {
        $laVue = new VueSaisieGroupe();
        $this->vue = $laVue;
        $laVue->setActionRecue("creer");
        $laVue->setActionAEnvoyer("validerCreer");
        $laVue->setMessage("Nouveau groupe");
        // En création, on affiche un formulaire vide
        /* @var Groupe $unGroup */
        $unGroupe = new Groupe("", "", "", "", "", "", "");
        $laVue->setUnGroupe($unGroupe);
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - groupes");
        $this->vue->afficher();
    }

    /** controleur= groupe & action=validerCreer
     * ajouter d'un groupe dans la base de données d'après la saisie    */
    public function validerCreer() {
        Bdd::connecter();
        /* @var Groupe $unGroupe  : récupération du contenu du formulaire et instanciation d'un groupe */
        $unGroupe = new Groupe($_REQUEST['id'], $_REQUEST['nom'], $_REQUEST['idResp'], $_REQUEST['adresse'], $_REQUEST['nbPers'], $_REQUEST['pays'], $_REQUEST['hebergement']);
        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de création (paramètre n°1 = true)
        $this->verifierDonneesGroup($unGroupe, true);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer le groupe
            GroupeDAO::insert($unGroupe);
            // revenir à la liste des groupes
            header("Location: index.php?controleur=groupes&action=liste");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de création
            $laVue = new VueSaisieGroupe();
            $this->vue = $laVue;
            $laVue->setActionRecue("creer");
            $laVue->setActionAEnvoyer("validerCreer");
            $laVue->setMessage("Nouveau groupe");
            $laVue->setUnGroupe($unGroupe);
            parent::controlerVueAutorisee();
            $laVue->setTitre("Festival - groupes");
            $this->vue->afficher();
        }
    }
    
        
     /** controleur= groupes & action=modifier $ id=identifiant du groupe à modifier
     * Afficher le formulaire de modification d'un groupe     */
    public function modifier() {
        $idGroupe = $_GET["id"];
        $laVue = new VueSaisieGroupe();
        $this->vue = $laVue;
        // Lire dans la BDD les données du groupe à modifier
        Bdd::connecter();
        /* @var Groupe $unGroupe */
        $unGroupe = GroupeDAO::getOneById($idGroupe);
        $this->vue->setUnGroupe($unGroupe);
        $laVue->setActionRecue("modifier");
        $laVue->setActionAEnvoyer("validerModifier");
        $laVue->setMessage("Modifier le groupe : " . $unGroupe->getNom() . " (" . $unGroupe->getId() . ")");
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - groupes");
        $this->vue->afficher();
    }

    /** controleur= groupes & action=validerModifier
     * modifier un groupe dans la base de données d'après la saisie    */
    public function validerModifier() {
        Bdd::connecter();
        /* @var Groupe $unGroupe : récupération du contenu du formulaire et instanciation d'un groupe */
        $unGroupe = new Groupe($_REQUEST['id'], $_REQUEST['nom'], $_REQUEST['idResp'], $_REQUEST['adresse'], $_REQUEST['nbPers'], $_REQUEST['pays'], $_REQUEST['hebergement']);
        
        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de modification (paramètre n°1 = false)
        $this->verifierDonneesGroup($unGroupe, false);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer les modifications pour le groupe
            GroupeDAO::update($unGroupe->getId(), $unGroupe);
            // revenir à la liste des groupes
            header("Location: index.php?controleur=groupes&action=liste");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de modification
            $laVue = new VueSaisieGroupe();
            $this->vue = $laVue;
            $laVue->setUnGroupe($unGroupe);
            $laVue->setActionRecue("modifier");
            $laVue->setActionAEnvoyer("validerModifier");
            $laVue->setMessage("Modifier le groupe : " . $unGroupe->getNom() . " (" . $unGroupe->getId() . ")");
            parent::controlerVueAutorisee();
            $laVue->setTitre("Festival - groupes");
            $this->vue->afficher();
        }
    }
    
    /**
     * Vérification des données du formulaire de saisie
     * @param Groupe $unGroupe groupe à vérifier
     * @param bool $creation : =true si formulaire de création d'un nouveau groupe ; =false sinon
     */
    private function verifierDonneesGroup(Groupe $unGroupe, bool $creation) {
        // Vérification des champs obligatoires.
        // Dans le cas d'une création, on vérifie aussi l'id
        if (($creation && $unGroupe->getId() == "") || $unGroupe->getNom() == "" || $unGroupe->getNbPers() == "" || $unGroupe->getNomPays() == "" ||
                $unGroupe->getHebergement() == "") {
            GestionErreurs::ajouter('Chaque champ suivi du caractère * est obligatoire');
        }
        // En cas de création, vérification du format de l'id et de sa non existence
        if ($creation && $unGroupe->getId() != "") {
            // Si l'id est constitué d'autres caractères que de lettres non accentuées 
            // et de chiffres, une erreur est générée
            if (!estAlphaNumerique($unGroupe->getId())) {
                GestionErreurs::ajouter("L'identifiant doit comporter uniquement des lettres non accentuées et des chiffres");
            } else {
                if (GroupeDAO::isAnExistingId($unGroupe->getId())) {
                    GestionErreurs::ajouter("Le groupe " . $unGroupe->getId() . " existe déjà");
                }
            }
           
        }
        // Vérification du format de l'hébergement (O/N)
        if ($unGroupe->getHebergement() != "O" && $unGroupe->getHebergement() != "N") {
            GestionErreurs::ajouter('L\'hébergement doit étre \'O\' si le groupe est hébergé, ou \'N\' si il ne l\'est pas.');
        }
        
    }
}
?>
